---
# Display name
name: Jason Huang

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: High School Student

# Organizations/Affiliations
organizations:

# Short bio (displayed in user profile at end of posts)
bio: Highschool student, prospective CS major

interests:
- Linux
- Information Security
- Physics

# education:
#   courses:
#   - course: PhD in Artificial Intelligence
#     institution: Stanford University
#     year: 2012
#   - course: MEng in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2009
#   - course: BSc in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/jhthenerd
- icon: github
  icon_pack: fab
  link: https://github.com/jhthenerd
- icon : gitlab
  icon_pack: fab
  link: https://gitlab.com/jhthenerd
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# user_groups:
# - Researchers
# - Visitors
---

Jason Huang is a high school student.

He is an executive of the VEX Robotics Club, the Computer Science Club, and an advisor for his school on 3D Printing.

His interests include Linux, programming, information security, and physics.

He is currently learning Go and Rust in his free time, as well as developing a few applications for his community.
