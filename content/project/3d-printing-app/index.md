---
title: 3D Printing App
summary: A user-centric 3D Printing app to allow for for students to submit models, and teachers to vet and update print requests
date: "2019-04-01"

links:
- icon: github
  icon_pack: fab
  name: Public Source
  url: https://github.com/davidli3100/3DPrintingPublic
---

This app was developed in the spring of 2019 as a collaborative effort between [David Li][david] and I

This was a proof-of-concept of a 3D Printing App for use internally at school to allow students to submit 3D Prints, and have them approved by teachers.

It would provide students with an easy way to submit prints, view the status of prints, and communicate with teachers to resolve issues
It would also allow teachers to view the models in the browser, and send them to the printer when ready.

[david]: https://github.com/davidli3100