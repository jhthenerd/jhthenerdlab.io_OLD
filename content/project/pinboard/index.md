---
title: Pinboard
summary: A school announcement, calendaring, and voting app
date: "2019-06-01"
---

An ongoing project to build an announcement replacement app for our school. Currently on backlog due to tense political climate within the school board and other projects.