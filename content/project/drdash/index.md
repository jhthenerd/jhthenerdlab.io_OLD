---
title: DrDash
summary: A voice and text based assistant to help doctors with appointments
date: "2019-09-15"
tags:
- Hackathon

links:
- icon: github
  icon_pack: fab
  name: Repo
  url: https://github.com/davidli3100/HTN2019
---

Our project for **Hack the North 2019**, an assistant app for both voice and text appointment booking.
Built using DialogFlow, this app is scalable to almost any existing assistant or chatbot service.
We built the appointment interface in React, and the backend was written in a combination of Python and Javascript.