---
title: Open Do
summary: A minimalistic CLI task manager
date: "2019-07-01"

# Optional external URL for project (replaces project detail page).
external_link: ""

links:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---

An ongoing project to build a minimalistic CLI task manager.
This project started as a GUI task manager, but shifted into CLI for both personal workflow benefit and simplicity.
The current version of the app is planned to be written in Rust, and have both a flag-based and a TUI-based interface.