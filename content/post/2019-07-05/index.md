---
title: 'First post!'
authors:
- admin
date: "2019-07-05"
---

Welcome to my blog!
I'll generally be posting content about technology and other things that I find interesting here.
I probably won't be updating this often, so feel free to follow my other social media for more up-to-date information about my life.
